package net.wizapps.kotlin.l_Generics

/**
 * Kotlin has meaning of "in" and "out" positions of generics:
 * out - return type(Covariance)
 * in - function parameters(Contravariance)
 *
 * When "out" position is set in generic declaration(Covariance),
 * generic type could accept only subclasses of declared type <out T : Flower>
 *
 * When "in" position is set in generic declaration(Contravariance),
 * generic type could accept only superclasses of declared type <in T : Rose>
 *
 * In other words when "out" is set in generic declaration,
 * we can use generic type only in out position(return type),
 * when "in" is set, in in position(function parameters).
 *
 * That's why you can't use generic as a function parameter,
 * when <out T : Flower> is set in generic declaration.
 *
 * Also we could have only "val" variables in constructor in covariance class(with "out" position)
 */
class Garden<out T : Flower>(val flowers: List<T>) {

    fun getFlower(): T = flowers[flowers.lastIndex]

//    fun plantFlower(flower: T) { // we can't use generic type as a parameter
//        flowers.add(flower)      // when "out" position is set in generic declaration
//    }

    /**
     * If you want to have function in covariance class(with out position),
     * witch takes generic as a parameter, you could add "@UnsafeVariance" annotation.
     * This annotation tells compiler: hey, don't worry, this parameter didn't change anything
     */
    fun doWeHaveTheSameFlower(flower: @UnsafeVariance T) {
        flowers.forEach { if (it == flower) println("We have already this flower") }
    }

    /**
     * If function in covariance class(with out position) is private,
     * we don't have to add "@UnsafeVariance" annotation, because this function can't change class from outside
     */
    private fun cutFlower(flower: T) {
        println("Cutting flower: $flower")
    }
}

open class Flower(val name: String)
class Rose : Flower("Rose")
class Chamomile : Flower("Chamomile")