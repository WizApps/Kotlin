package net.wizapps.kotlin.a_Basics

fun main(args: Array<String>) {
    aliasSample()
}

/** Custom alias for any package or class */
typealias EmployeeList = ArrayList<Employee>

fun aliasSample() {
    val employeeList = EmployeeList()
    employeeList.add(Employee("Frodo", 333))
    employeeList.add(Employee("Sam", 111))
    println(employeeList)
}