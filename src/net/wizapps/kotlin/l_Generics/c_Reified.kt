package net.wizapps.kotlin.l_Generics

import java.math.BigDecimal

fun main(args: Array<String>) {
    val mixedList = listOf("Yep", false, 12, BigDecimal(1543.1554), 245.54, "What", true, 32)
    val numbers = getElementsOfType<Int>(mixedList)
    val strings = getElementsOfType<String>(mixedList)
    println(numbers)
    println(strings)
}

/**
 * Only if you want to check generic type in function,
 * you should add "reified" in couple with "inline" operator
 */
inline fun <reified T> getElementsOfType(list: List<Any>): List<T> {
    val someList = mutableListOf<T>()
    list.forEach {
        if (it is T) {
            someList.add(it)
        }
    }
    return someList
}