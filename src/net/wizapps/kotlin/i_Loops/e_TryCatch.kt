package net.wizapps.kotlin.i_Loops

fun main(args: Array<String>) {
    println(getNumber("Frodo"))
}

fun getNumber(string: String): Int {
    /** You can use "try/catch" block as an expression as well */
    return try {
        Integer.parseInt(string)
    } catch (ex: NumberFormatException) {
        0 // this value will returns
    } finally {
        /** Finally block in Kotlin returns nothing(Unit) */
        println("I'm in the finally block")
    }
}