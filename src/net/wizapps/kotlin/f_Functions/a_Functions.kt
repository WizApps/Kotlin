package net.wizapps.kotlin.f_Functions

import net.wizapps.kotlin.e_DataClasses.Car

fun main(args: Array<String>) {
    println(multiply(3, 4, "The result is:"))
    println(multiplySimpler(3, 4, "The result is:"))
    withoutReturningType()
    Character("Frodo").nameToUpperCase()
    defaultParameter("Ihor")
    defaultParameter("Ihor", 30)

    /** If we put one name for parameter order, we should add labels for other parameters */
    parameterOrder(secondInt = 22, firstInt = 11)

    val car1 = Car("Audi", "Blue", 2008)
    val car2 = Car("BMW", "Black", 2010)
    val car3 = Car("Fiat", "White", 2015)
    printCarColor(car1, car2, car3)

    /** Added label for Int value for specify witch value is not "vararg" */
    fewParametersWithVararg(car1, car2, car3, someInt = 11)

    /** Sample with spread operator */
    manyCars()
}

/**
 * - In Kotlin you could create functions without adding them inside the class(high order function)
 * - Every function in Kotlin without return type implicitly returns "Unit" type
 * - Every function in Kotlin is public and final by default
 * - In Kotlin we shouldn't follow the parameters order if we specify parameter name
 */

/** This function with "Block" body {} */
fun multiply(operand1: Int, operand2: Int, label: String): String {
    return ("$label ${operand1 * operand2}")
}

/**
 * Simplify upper function.
 * This function with "Expression" body(without curly braces).
 * Current expressions available only for one line, or one expression code.
 * It's implicitly returns String
 */
fun multiplySimpler(operand1: Int, operand2: Int, label: String) = "$label ${operand1 * operand2}"

/** Sample without returning type(returns Unit) */
fun withoutReturningType() = println("Function without returning type")

/** Sample with class */
class Character(var charName: String) {
    fun nameToUpperCase() = println(charName.toUpperCase())
}

/**
 * Default parameters for avoiding overloaded functions.
 * Will generates for us under the hood two overloaded functions
 */
fun defaultParameter(firstName: String, age: Int = 22) = println("First name: $firstName, age: $age")

/** Sample with parameters order(look at realization in main()) */
fun parameterOrder(firstInt: Int, secondInt: Int) {
    println("First Int is = $firstInt, second Int is = $secondInt")
}

/**
 * If we want to add unlimited number of some values or objects,
 * we could have only "vararg" parameter in function
 */
fun printCarColor(vararg cars: Car) {
    for (car in cars) println(car.color)
}

/**
 * If we have few arguments with "vararg" parameter, we should add parameter labels when we call this function,
 * watch realization in main()
 */
fun fewParametersWithVararg(vararg cars: Car, someInt: Int) {}

/** Spread operator */
fun manyCars() {
    val car1 = Car("Audi", "Blue", 2008)
    val car2 = Car("BMW", "Black", 2010)
    val car3 = Car("Fiat", "White", 2015)
    val threeCars = arrayOf(car1, car2, car3)
    val car4 = car2.copy()
    val car5 = car3.copy(model = "Mercedes Benz", year = 2017)

    /**
     * Spread operator "*" spreads array to single objects,
     * or in other words, unpack elements from an array
     */
    val manyCars = arrayOf(*threeCars, car4, car5)
    for (i in manyCars) println(i)
}