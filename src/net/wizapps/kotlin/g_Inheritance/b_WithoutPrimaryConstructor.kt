package net.wizapps.kotlin.g_Inheritance

fun main(args: Array<String>) {
    Lion("King", 22)
}

/** Inheritance without a primary constructor */
open class Animal {

    private val animalName: String

    constructor(animalName: String) {
        this.animalName = animalName
    }
}

/** Silly sample but it's only for visualizing :) */
class Lion : Animal {

    private val age: Int

    constructor(animalName: String, age: Int) : super(animalName) {
        this.age = age
        println("Animal name: $animalName\nAnimal age: $age")
    }
}