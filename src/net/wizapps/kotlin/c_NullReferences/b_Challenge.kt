package net.wizapps.kotlin.c_NullReferences

import net.wizapps.kotlin.o_JavaClasses.DummyMethods

fun main(args: Array<String>) {
    challengeNullReference()
}

fun challengeNullReference() {
    /**
     * Declare a non-nullable float variables in three ways,
     * and assign it value to -8347.643
     */
    val float1 = -8347.643f
    val float2: Float = -8347.643f
    val float3 = (-8347.643).toFloat()

    /** Declare variables with nullable types */
    var float4: Float? = 5643.6432f
    val float5: Float? = null

    /**
     * Declare an array of type not-nullable Short.
     * Make it size 5, and assign it the values 1,2,3,4,5
     */
    val shortArray1 = shortArrayOf(1, 2, 3, 4, 5)
    val shortArray2: Array<Short> = arrayOf(1, 2, 3, 4, 5)
    val shortArray3 = Array(5) { i -> (i + 1).toShort() }

    /**
     * Declare an array of nullable Ints and initialize it with the values:
     * 5, 10, 15, 20, 25, 30, 35, 40, 45, 50... 200
     */
    val nullableArray = Array<Int?>(40) { i -> (i + 1) * 5 }

    /**
     * Call Java method with the following signature from Kotlin:
     * public void printChars(char[] chars)
     * Declare an array that you could pass to current method
     */
    val charArray1 = arrayOf('f', 'r', 'o', 'd', 'o')
    val charArray2 = charArrayOf('i', 'h', 'o', 'r')
    DummyMethods().printChars(charArray1.toCharArray())
    DummyMethods().printChars(charArray2)

    /** Given the following code: */
    val text: String? = "I AM IN UPPER CASE"
    /**
     * Using one line of code, declare another String variable,
     * and apply x.toLoverCase() when "text" isn't null,
     * and the String "I am null!" when "text" is null
     */
    val y = text?.toLowerCase() ?: "I am null!"
    println(y)

    /**
     * Use let() function to "text" for:
     * a) lowercase the String
     * b) replace "am" with "am not" in the result
     */
    text?.let {
        val temp = it.toLowerCase().replace("am", "am not")
        println(temp)
    }

    /**
     * You are really, really confident that variable "notNullVariable" can't contain null?
     * Change the following code to assert that "notNullVariable" isn't null
     */
    val notNullVariable: Int? = 11
    println(notNullVariable!!.toDouble())
}