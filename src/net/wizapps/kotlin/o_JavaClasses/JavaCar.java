package net.wizapps.kotlin.o_JavaClasses;

public class JavaCar {

    private static int count = 1;
    private String name;
    private String color;
    private int year;

    public JavaCar(String name, String color, int year) {
        this.name = name;
        this.color = color;
        this.year = year;
    }

    public static int getCount() {
        return count++;
    }

    public static void startNewThread(Runnable runnable) {
        new Thread(runnable).start();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void printStrings(String... strings) {
        for (String string : strings) {
            System.out.println(string);
        }
    }

    @Override
    public String toString() {
        return "JavaCar{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", year=" + year +
                '}';
    }
}