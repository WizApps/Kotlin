package net.wizapps.kotlin.m_FileIO

import java.io.File

fun main(args: Array<String>) {
    readByLines()
    readAllString()
    forEachLine()
}

fun readByLines() {
    /** File().readLines() will return the List of Strings(List<String>) */
    val allLinesInFile = File("textFileForReading.txt").readLines()
    allLinesInFile.forEach { println(it) }
}

fun readAllString() {
    /** File().readText() will return String with all text file */
    val stringFromFile = File("textFileForReading.txt").readText()
    println(stringFromFile)
}

fun forEachLine() {
    File("textFileForReading.txt").forEachLine { println(it) }
}