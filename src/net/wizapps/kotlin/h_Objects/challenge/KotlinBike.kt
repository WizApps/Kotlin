package net.wizapps.kotlin.h_Objects.challenge

fun main(args: Array<String>) {

    /** If we have default value for some property in the middle of constructor, we should add labels for all parameters */
    val bike = KotlinBike(cadence = 12, speed = 40)
    val mountainBike = KotlinBikeMountain(cadence = 10, speed = 30, seatHeight = 20)
    val roadBike = KotlinBikeRoad(15, 24, 60, 10)

    bike.bikeDescription()
    mountainBike.bikeDescription()
    roadBike.bikeDescription()

    KotlinBikeMountain.colors.forEach { println(it) }
}

/** Kotlin analog for Java Bike class */
open class KotlinBike(var cadence: Int, var gear: Int = 20, var speed: Int) {

    fun applyBreak(decrement: Int) {
        speed -= decrement
    }

    fun speedUp(increment: Int) {
        speed += increment
    }

    open fun bikeDescription() {
        println("Bike is in gear $gear with a cadence of $cadence travelling a speed of $speed.")
    }
}

/** Kotlin analog for Java BikeMountain class */
class KotlinBikeMountain(cadence: Int, gear: Int = 12, speed: Int, var seatHeight: Int) : KotlinBike(cadence, gear, speed) {

    companion object {
        val colors = listOf("blue", "red", "white", "green", "purple", "yellow") // or arrayOf()
    }

    override fun bikeDescription() {
        super.bikeDescription()
        println("The mountain bike has a seat height of $seatHeight.")
    }
}

/** Kotlin analog for Java BikeRoad class */
class KotlinBikeRoad(cadence: Int, gear: Int, speed: Int, private val tireWith: Int) : KotlinBike(cadence, gear, speed) {

    override fun bikeDescription() {
        super.bikeDescription()
        println("The road bike has a tire width of $tireWith.")
    }
}