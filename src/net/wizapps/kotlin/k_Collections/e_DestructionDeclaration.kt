package net.wizapps.kotlin.k_Collections

fun main(args: Array<String>) {
    destructionDeclaration()
}

fun destructionDeclaration() {
    /** Pair sample */
    val pair = Pair("ten", 10)
    println("${pair.first}, ${pair.second}")

    /** Triple sample */
    val triple = Triple(false, 12.54, "Yes")
    println("${triple.first}, ${triple.second}, ${triple.third}")

    /** Triple to mutable List */
    val tripleList = triple.toList().toMutableList()
    tripleList.add(tripleList.lastIndex + 1, true)
    println(tripleList)

    /** Destructuring declaration sample */
    val (firstVal, secondVal, thirdVal) = triple
    println("$firstVal, $secondVal, $thirdVal")

    /** Destructuring declaration with class */
    val character = Character("Legolas", "Elf", 62)
    val (name, race, level) = character
    println("name = $name, race = $race, level = $level")

    /** Destructuring declaration with data class */
    val worker = Worker("Ihor", "Choliy", 15000)
    val (firstName, secondName, salary) = worker
    println("firstName = $firstName, secondName = $secondName, salary = $salary")
}

class Character(val name: String, val race: String, val level: Int) {

    /**
     * For availability using Character class as destruction component.
     * Only public variables available as destruction component.
     * You could add any number of components what you need
     */
    operator fun component1() = name
    operator fun component2() = race
    operator fun component3() = level
}

/**
 * For data classes You shouldn't add "component()" functions
 * if you want to use them as destruction component
 */
data class Worker(val firstName: String, val secondName: String, val salary: Int)