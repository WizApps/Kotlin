package net.wizapps.kotlin.b_DataTypes

import net.wizapps.kotlin.o_JavaClasses.DummyMethods
import java.util.*

fun main(args: Array<String>) {
    dataTypesSample()
}

fun dataTypesSample() {
    /** Default numeric value is Int */
    val myInt = 11

    /** Long dataType */
    var myFirstLong = 22L
    var mySecondLong: Long = 33

    /**
     * You can't assign in Kotlin:
     * myFirstLong = myInt
     * You should convert Int to Long
     */
    myFirstLong = myInt.toLong()

    val myByte: Byte = 111
    var myShort: Short = 222
    myShort = myByte.toShort()

    /** Double dataType */
    val myFirstDouble = 12.3432
    val mySecondDouble: Double = 34.543 // redundant
    println("Is this one is double: ${myFirstDouble is Double}")

    /** Float dataType */
    val myFirstFloat = 553.646f
    val mySecondFloat: Float = 45.154.toFloat()

    /** Char dataType */
    val myFirstChar = 'B'
    val mySecondChar = 65.toChar()
    println("$myFirstChar + $mySecondChar")

    /** Boolean dataType */
    val isVacation = false
    val onVacation = DummyMethods().isVacationTime(getRandomBoolean())
    println(onVacation)
}

private fun getRandomBoolean(): Boolean {
    val random = Random()
    return random.nextBoolean()
}