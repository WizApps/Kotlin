package net.wizapps.kotlin.b_DataTypes

import net.wizapps.kotlin.o_JavaClasses.DummyMethods

fun main(args: Array<String>) {
    arraysSample()
}

fun arraysSample() {
    /** String array */
    val stringArray = arrayOf("First", "Second", "Third")

    /** Long array */
    val longArrayFirst = arrayOf(1L, 2L, 3L)
    val longArraySecond = arrayOf<Long>(1, 2, 3)

    /** Array length is 7 */
    val customLengthArray = Array(7) { i -> i * 2 } // this lambda fills all array

    /** First variant of printing Array */
    for (number in customLengthArray) {
        println(number)
    }

    /** Second variant of printing Array */
    customLengthArray.forEach { print("$it + / ") }

    /** Initialize huge array */
    val lotOfNumbers = Array(100000) { i -> (i + 1) * 10 }

    /** Initialize array with the same values */
    val allZerosArray = Array(10) { 0 }

    /** Mixed array */
    val mixedArray = arrayOf(1, "Fists", true, 25.6433, 'b')
    for (element in mixedArray) {
        println(element)
    }
    mixedArray.forEach { print("$it + / ") }

    /** Creating empty array with current size */
    var emptyArray = IntArray(5) // in Kotlin array you can't do like that "Array(5)"

    /** Passing Int array to Java array (can't pass arrayOf()) */
    val intArray = intArrayOf(4, 643, 7, 32, 246, 2)
    DummyMethods().printNumbers(intArray)

    /** Convert Kotlin Array to Java Array */
    val kotlinArray = arrayOf(1, 2, 3)
    DummyMethods().printNumbers(kotlinArray.toIntArray())
    DummyMethods().printNumbers(intArray) // or this one which is created as intArrayOf(...)

    /** Convert Java Array to Kotlin Array */
    val javaArray = longArrayOf(1, 2, 3)
    var newKotlinArray: Array<Long> = javaArray.toTypedArray()
    var veryNewKotlinArray = javaArray.toTypedArray() // simpler way, without adding type Array<Long> after variable name
}