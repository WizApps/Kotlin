package net.wizapps.kotlin.n_JavaInteroperability;

import java.io.IOException;

public class c_CallKotlin {

    public static void main(String[] args) {
        getTopLevelFunction();
        getValuesFromKotlinObject();
        companionAccess();
        singletonAccess();
        catchKotlinException();
        defaultParameters();
    }

    private static void getTopLevelFunction() {
        /**
         * If you want to call Kotlin high level functions from Java code
         * You should call them like a companion object, class name + Kt suffix.
         * Or you could add unique name to any class(watch at the top of bKotlinCode class)
         */
        //BKotlinCodeKt.topLevelFunction();
        UniqueName.topLevelFunction();
    }

    private static void getValuesFromKotlinObject() {
        /**
         * You could change Kotlin variables with get() and set() function
         * if that fields is not val or private.
         * Or if @JvmField annotation added before variable, you could call it without getters and setters
         */
        Avatar avatar = new Avatar("Elfigor", "Elf", 109, true);
        avatar.setName("Modeler");
        avatar.setRace("Orc");
        avatar.level = 110;
        System.out.println(avatar);
    }

    private static void companionAccess() {
        Avatar.Companion.companionFunction();

        /** With @JvmStatic annotation */
        Avatar.jvmStaticFunction();

        System.out.println(Avatar.hasIllnesses);

        /** Without @JvmField annotation */
        System.out.println(Avatar.Companion.getHasCurses());

        /** If you want to get constant from Companion object you could call it without calling Companion instance */
        System.out.println(Avatar.constant);
    }

    private static void singletonAccess() {
        /** Without @JvmStatic annotation */
        KotlinSingleton.INSTANCE.singletonFunction();

        /** With @JvmStatic annotation */
        KotlinSingleton.singletonFunction();
    }

    private static void catchKotlinException() {
        try {
            /** Go to the function declaration and read info above */
            UniqueName.throwsIO();
        } catch (IOException ex) {
            System.out.println("Captured and IOException");
        }
    }

    private static void defaultParameters() {
        /** Go to the function declaration and read info above */
        UniqueName.printArgs("Frodo");
        UniqueName.printArgs("Gendalf", 426);
    }
}