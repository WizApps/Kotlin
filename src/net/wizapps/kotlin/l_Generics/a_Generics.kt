package net.wizapps.kotlin.l_Generics

fun main(args: Array<String>) {

    /** You could add generic type "MutableList<String>" to variable or not */
    val strings: MutableList<String> = mutableListOf("Yes", "No", "Ok", "Maybe")
    val numbers = mutableListOf(1, 2, 3, 4, 5, 6, 7)

    printCollection(strings)
    numbers.printList()
}

/** Simple function example with generics */
fun <T> printCollection(collection: List<T>) {
    collection.forEach { println(it) }
}

/** Simple Extension function example with generics */
fun <T> List<T>.printList() {
    this.forEach { println(it) }
}