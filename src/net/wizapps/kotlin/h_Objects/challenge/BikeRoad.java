package net.wizapps.kotlin.h_Objects.challenge;

public class BikeRoad extends Bike {

    private int tireWidth;

    public BikeRoad(int cadence, int gear, int speed, int tireWidth) {
        super(cadence, gear, speed);
        this.tireWidth = tireWidth;
    }

    public int getTireWidth() {
        return tireWidth;
    }

    @Override
    public void bikeDescription() {
        super.bikeDescription();
        System.out.println("The road bike has a tire width of " + tireWidth + ".");
    }
}