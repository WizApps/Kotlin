package net.wizapps.kotlin.m_FileIO

import java.io.File

fun main(args: Array<String>) {
    showAllFiles()
    showOnlyKotlinFiles()
    walkInFileTree()
}

/** Shows all files and folders in this directory */
fun showAllFiles() {
    File(".")
            .walkTopDown()
            .forEach { println(it) }
}

fun showOnlyKotlinFiles() {
    File(".")
            .walkTopDown()
            .filter { it.name.endsWith(".kt") }
            .forEach { println(it) }
}

fun walkInFileTree() {
    File("./src/")
            .walk(FileWalkDirection.BOTTOM_UP)
            .forEach { println(it) }
}