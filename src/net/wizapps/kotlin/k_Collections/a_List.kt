package net.wizapps.kotlin.k_Collections

fun main(args: Array<String>) {
    listTypes()
}

fun listTypes() {
    /**
     * List in Kotlin it's by default is immutable.
     * You can change objects in List, but it would be the new instance of the List.
     * You can't add or remove objects from immutable list in Kotlin,
     * only if you pass it to Java code.
     */
    val stringList = listOf("winter", "spring", "summer", "autumn")
    println(stringList.javaClass)

    /** You could print values in list just like that */
    println(stringList)

    /** Few ways to create empty immutable list */
    val emptyList1 = emptyList<String>()
    val emptyList2 = listOf<String>()
    println(emptyList1.javaClass)
    println(emptyList2.javaClass)

    /** Not null list sample */
    val notNullList = listOfNotNull("Hello", null, null, "Yes", null)
    println(notNullList)

    /** Mutable Java ArrayList */
    val arrayList = arrayListOf(1, 2, 3, 4, 5)
    val mutableList = mutableListOf(5, 4, 3, 2, 1)
    println(arrayList.javaClass)
    println(mutableList.javaClass)

    /** Changing items in mutable list */
    mutableList[1] = 22
    mutableList.set(4, 40)
    println(mutableList)

    /** From List immutable to mutable */
    val mutableStrings = stringList.toMutableList()
    mutableStrings.add(mutableList.lastIndex, "srotem")
    println(mutableStrings)

    /** Array to List */
    val array = arrayOf("red", "green", "blue")
    val firstWayToConvert = listOf(*array) // using "spread" operator to unpack array
    val secondWayToConvert = array.toList()
    println(firstWayToConvert)
    println(secondWayToConvert)
}