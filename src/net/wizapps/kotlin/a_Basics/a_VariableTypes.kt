package net.wizapps.kotlin.a_Basics

fun main(args: Array<String>) {
    variableSamples()
}

fun variableSamples() {
    /** Immutable (first variant) */
    val numberOne: Int
    numberOne = 11
    println(numberOne)

    /** Immutable (second variant) */
    val numberTwo = 22
    println(numberTwo)

    /** Immutable (third variant, not recommended) */
    val numberThree: Int = 33
    println(numberThree)

    /** Mutable (recommends use in most cases immutable types "val") */
    var numberFour = 44
    numberFour = 55
    println(numberFour)

    /** Class (variables in the immutable class could be mutable, if they are "var") */
    val employee = Employee("Ihor Choliy", 1)
    employee.name = "Igor Cholii"
    println(employee.name)
}