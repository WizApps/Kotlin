package net.wizapps.kotlin.k_Collections

fun main(args: Array<String>) {
    println(sequences())
}

/**
 * Sequences(Streams in Java) in Kotlin recommended using only for huge data, more 100 000 items.
 * Kotlin collections is very efficient and fast, that's why use sequences only for very big data structures.
 * Sequences works only with one item per time for all filters, after that takes another one (like streams)
 */
fun sequences() =
        listOf("Joe", "Mary", "Jane", "Jerry", "Ann").asSequence()
                .filter {
                    println("filtering $it")
                    it.startsWith("J")
                }.map {
                    println("mapping $it")
                    it.toUpperCase()
                }.onEach {
                    println("adding i to $it\n")
                }.toList() // sequence executes only if end function will be terminal