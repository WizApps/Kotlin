package net.wizapps.kotlin.j_Lambas.java;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class b_JavaFunctionalType {

    public static void main(String[] args) {
        List<Person> persons = Person.getPersons();

        // Step 1: Sort list by last name
        persons.sort((o1, o2) -> o1.getLastName().compareTo(o2.getLastName()));

        // Step 2: Create a method that prints all elements in list
        printAllElements(persons, b_JavaFunctionalType::printMessage);
        // bJavaFunctionalType::printMessage | the same as | person -> printMessage(person)

        // Step 3: Create a method that prints all people that have name beginning with "C"
        printConcretePerson(persons, person -> person.getLastName().startsWith("C"));
    }

    private static void printAllElements(List<Person> persons, Consumer<Person> consumer) {
        System.out.println("Sorted list of Persons:");
        for (Person person : persons) {
            consumer.accept(person);
        }
    }

    private static void printConcretePerson(List<Person> persons, Predicate<Person> predicate) {
        System.out.println("\nPersons witch starts with concrete letter:");
        for (Person person : persons) {
            if (predicate.test(person)) {
                System.out.println(person);
            }
        }
    }

    private static void printMessage(Person person) {
        System.out.println(person);
    }
}