package net.wizapps.kotlin.e_DataClasses

fun main(args: Array<String>) {
    val toyota1 = Car("Toyota", "White", 2015)
    /** DataClass toString() function in action */
    println(toyota1)

    val toyota2 = Car("Toyota", "White", 2015)
    /** DataClass equals() function in action */
    println(toyota1 == toyota2)

    /** DataClass copy() function in action */
    val toyota3 = toyota2.copy(color = "Blue", year = 2017)
    println(toyota3)
}

/**
 * - DataClass has overridden toString(), hashCode(), equals(), copy() functions
 * - DataClass should have at least one field in constructor
 * - All dataClass fields should have "var" or "val" declaration
 * - DataClass could be abstract or inner
 */
data class Car(var model: String, var color: String, var year: Int)