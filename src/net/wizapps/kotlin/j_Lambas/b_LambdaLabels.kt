package net.wizapps.kotlin.j_Lambas

fun main(args: Array<String>) {
    findEmployeeByIdTraditional(11)
    findEmployeeById(7)
    lambdaLabels()
}

/** Function without lambda */
fun findEmployeeByIdTraditional(id: Int) {
    for (employee in getEmployees()) {
        if (employee.id == id) {
            println("Yes, there is an employee with the id: $id")
            return
        }
    }
    println("Nope, there is no employee with the id: $id")
}

/** Function with lambda */
fun findEmployeeById(id: Int) {
    getEmployees().forEach lambdaLabel@{
        if (it.id == id) {
            println("Yes, there is an employee with the id: $id")

            /** If you want to return only from lambda but not from all function, you should add lambda label */
            return@lambdaLabel
        }
    }
    /** This one will be printed as well, because we have returned only from lambda */
    println("Nope, there is no employee with the id: $id")
}

/** Another sample with Lambdas labels, if we want to get upper object in nested lambda */
fun lambdaLabels() {
    "Some String".apply someString@{
        "Another String".apply {
            println(toLowerCase())
            println(this@someString.toUpperCase())
        }
    }
}