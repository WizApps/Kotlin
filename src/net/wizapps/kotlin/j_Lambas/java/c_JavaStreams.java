package net.wizapps.kotlin.j_Lambas.java;

public class c_JavaStreams {

    public static void main(String[] args) {
        Person.getPersons().stream()
                .filter(person -> person.getLastName().startsWith("C"))
                .forEach(System.out::println);
    }
}