package net.wizapps.kotlin.k_Collections

import net.wizapps.kotlin.e_DataClasses.Car

fun main(args: Array<String>) {
    mapsType()
}

fun mapsType() {
    /** Immutable Map */
    val immutableMap = mapOf(
            1 to Car("Audi", "Blue", 2008),
            2 to Car("BMW", "Black", 2011),
            3 to Car("Ford", "White", 2003))
    println(immutableMap)

    /** Mutable Map */
    val mutableMap = mutableMapOf( // or -> hashMapOf()
            "Frodo" to Car("Audi", "Blue", 2008),
            "Sam" to Car("BMW", "Black", 2011),
            "Pippin" to Car("Ford", "White", 2003))

    /** Changing items in mutable map */
    mutableMap["Legolas"] = Car("Ferrari", "Red", 2015)
    mutableMap.put("Aragorn", Car("Opel", "Blue", 1985))
    println(mutableMap)

    /** Getting Key and Value in Kotlin Map */
    mutableMap.forEach {
        println(it.key)
        println(it.value)
    }
}