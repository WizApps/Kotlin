package net.wizapps.kotlin.h_Objects

import java.time.Year

fun main(args: Array<String>) {

    /**
     * - Singletons calls from object (like static methods in Java - Singleton.someMethod())
     * - Singletons creates only in 11 line, next one is used the same object
     */
    println(FrodoSingleton.currentYear)
    println(FrodoSingleton.getFrodoInfo())
    println(FrodoSingleton.getFrodoAge())
    println(FrodoSingleton.getFrodoBestFriend())
}

/**
 * Singleton sample
 * - singletons couldn't have constructors
 * - singletons could extends other classes or interfaces
 */
object FrodoSingleton {

    val currentYear = Year.now().value

    fun getFrodoInfo() = "I'm Frodo Baggins from Shire!"
    fun getFrodoAge() = 47
    fun getFrodoBestFriend() = "Sam is the best Frodo's friend!"
}