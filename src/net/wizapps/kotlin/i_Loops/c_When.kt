package net.wizapps.kotlin.i_Loops

import net.wizapps.kotlin.d_Constructors.Player

fun main(args: Array<String>) {
    simpleWhen()
    whenWithIs()
    whenWithEnums()
    whenWithIfElse(10, 12)
}

fun simpleWhen() {
    val number = 200

    /** Alternative of java "switch/case" */
    when (number) {
        100 -> println("100")
        200 -> println("200")
        300 -> println("300")
        400 -> println("400")
        else -> println("Doesn't match anything")
    }

    val x = 20
    when (number) {
        100, 200 -> println("some logic") // means 100 or 200
        in 300..400 -> println("in range of 300..400") // means value is in current range
        x + 150 -> println("yep!")
        else -> println("Doesn't match anything")
    }
}

fun whenWithIs() {
    val obj1 = "I'm a String"
    val obj2 = Player("Elfigor", "Elf", 110)
    val obj3 = 123.45
    val unknownType: Any = obj2

    /** We can use "when" as a expression too, like "if/else" */
    val knownType = when (unknownType) {
        is String -> {
            println(unknownType.toUpperCase()) // smart casting in action
            1 // last value in etch block is that what expression returns
        }
        is Player -> {
            println(unknownType.nickname)
            2
        }
        is Double -> {
            println(unknownType.toInt())
            3
        }
        else -> {
            println("I have no idea what type this is...")
            -1
        }
    }
    println(knownType)
}

enum class Seasons {
    SPRING, SUMMER, AUTUMN, WINTER
}

fun whenWithEnums() {
    val unknownSeason = Seasons.SUMMER
    println(unknownSeason.javaClass)

    /** If we are using when expression with enums we don't need "else" block */
    val season = when (unknownSeason) {
        Seasons.SPRING -> "Flowers are blooming"
        Seasons.SUMMER -> "It's hot!"
        Seasons.AUTUMN -> "It's getting cooler"
        Seasons.WINTER -> "I need a coat!"
    }
    println(season)
}

fun whenWithIfElse(num1: Int, num2: Int) {

    /** If/Else example */
    if (num1 > num2) {
        println("num1 is greater then num2")
    } else if (num1 < num2) {
        println("num1 is less then num2")
    } else {
        println("num1 equals num2")
    }

    /** When example */
    when {
        num1 > num2 -> println("num1 is greater then num2")
        num1 < num2 -> println("num1 is less then num2")
        else -> println("num1 equals num2")
    }
}