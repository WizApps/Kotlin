package net.wizapps.kotlin.h_Objects

fun main(args: Array<String>) {
    Department.HR.getDepartmentInfo()
    Department.IT.getDepartmentInfo()
    Department.ACCOUNT.getDepartmentInfo()
    Department.SALES.getDepartmentInfo()
}

enum class Department(private val departmentName: String, private val employeesCount: Int) {
    HR("Human resources", 5),
    IT("Information technology", 23),
    ACCOUNT("Accounting", 3),
    SALES("Sales managers", 4);

    fun getDepartmentInfo() = println("The department $departmentName has $employeesCount employees")
}