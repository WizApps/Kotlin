package net.wizapps.kotlin.i_Loops

fun main(args: Array<String>) {
    forExample()
    loopNameExample()
}

fun forExample() {
    /** Common "for each" loop */
    val seasons = arrayOf("winter", "spring", "summer", "autumn")
    for (index in seasons.indices) {
        println("${seasons[index].toUpperCase()} season index is: $index")
    }

    /** Arrays forEach loop */
    seasons.forEach { println(it.toUpperCase()) }

    /** Arrays indexed for loop */
    seasons.forEachIndexed { index, value -> println("${value.toUpperCase()} season index is: $index") }

    /** For loop with ranges */
    for (i in seasons.indices step 2) println(seasons[i].toUpperCase())

    /** For loop with backwards ranges */
    for (num in seasons.size downTo 0 step 2) println(num)
}

fun loopNameExample() {
    for (a in 1..3) {
        println("a = $a")
        bLoop@ for (b in 1..4) {
            println("b = $b")
            for (c in 1..5) {
                println("c = $c")
                /** If c == 3 @bLoop will breaks. We can use the same with "continue" keyword */
                if (c == 3) break@bLoop
            }
        }
    }
}